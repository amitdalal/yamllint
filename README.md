# yamllint

Gitlab CI for yamllint

- runs over git push when files with .yml or .yaml extensions change for any branch/tag
- allowed to fail

### For linting : [yamllint](https://yamllint.readthedocs.io/) is used. 

### How to use :

Include in the .gitlab-ci.yml.
It is provided as a hidden job (name .yamllint) so now it can be extended in any job in any stage of a pipeline.

```yaml
include:
  - project: 'amitdalal/yamllint'
    file: 'ci.yml'

yamllint:
  stage: test
  extends: .yamllint
  allow_failure: false

```
